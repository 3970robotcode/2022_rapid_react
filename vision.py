import cscore
import cv2
import numpy as np
import traceback

def main():
    usbCamera = cscore.UsbCamera("Camera_0", 0)
    usbCamera.setResolution(320, 240)

    server = cscore.MjpegServer("Output", 1181)

    cvSink = cscore.CvSink("USB_Camera")
    cvSink.setSource(usbCamera)

    w = int(160)
    h = int(120)
    mode = cscore.VideoMode(cscore.VideoMode.PixelFormat(1), w, h, 30)
    outputStream = cscore.CvSource("Name", mode)

    server.setSource(outputStream)

    f = np.zeros(shape=(w, h, 3), dtype=np.uint8)

    while True:
        time, frame = cvSink.grabFrame(f)
        if time == 0:
            # Send the output the error.
            outputStream.notifyError(cvSink.getError());
            # skip the rest of the current iteration
            continue

        out = cv2.resize(frame, (w, h), interpolation = cv2.INTER_NEAREST)

        outputStream.putFrame(out)

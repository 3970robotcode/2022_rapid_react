import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class autoY(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        #States Are To Be Put Into This Dict
        self.states = {
            "state1":self.state1,
            "state2":self.state2,
            "state3":self.state3,
            "state4":self.state4,
            "state5":self.state5,
            "state6":self.state6,
            "state7":self.state7,
            "state8":self.state8,
            "state9":self.state9,
            "state10":self.state10,
            "endState":self.endState
        }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.startTime = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
            "state1": False,
            "state2": False,
            "state3": False,
            "state4": False,
            "state5": False,
            "state6": False,
            "state7": False,
            "state8": False,
            "state9": False,
            "state10": False,
            "endState": False
        }

    def onFirstRun(self, *args, usesTiming = False):
        if not self.hasStateBeenRun[self.state]:
            if usesTiming:
                self.startTime = time.time()
            for method in args:
                method()
            self.hasStateBeenRun[self.state] = True

    def reset(self):
        self.robot.drivetrain.encoderAndGyroReset()
        self.robot.drivetrain.stopAllMotors()

        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}

        self.state = self.initialState

        print(self.state)

        print("Sensor Position", self.robot.drivetrain.rMaster.getSelectedSensorPosition())

    def state1(self):
        self.onFirstRun(self.robot.drivetrain.encoderAndGyroReset, usesTiming = True)

        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)

        nextState = self.robot.drivetrain.driveToPosition(-36)
        #nextState = self.robot.drivetrain.turnToDegree(90)
        #nextState = self.robot.drivetrain.driveToPositionVelocity(-45, 10000)
        print("(AutoX, state1) Next State", nextState)
        if  nextState:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state2"
            #self.state = "endState"
    def state2(self):
        self.onFirstRun(usesTiming = True)
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)
        self.robot.shooter.shoot(True)
        if  self.robot.drivetrain.driveToPosition(60) and time.time() - self.startTime > 3:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        self.onFirstRun(usesTiming = True)
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)
        self.robot.shooter.shoot(True)
        if  self.robot.stager.autoShoot(self.startTime, 3, self.robot.shooter.topError, self.robot.shooter.botError):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state4"

    def state4(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)

        if  self.robot.drivetrain.driveToPosition(-15):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state5"

    def state5(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)

        if  self.robot.drivetrain.turnToDegree(78):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state6"

    def state6(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(True, False)
        if  self.robot.drivetrain.driveToPosition(-230):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state7"

    def state7(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(True, False)
        if  self.robot.drivetrain.turnToDegree(-5):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state8"

    def state8(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(True, False)
        if  self.robot.drivetrain.driveToPosition(230):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state9"

    def state9(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(False, False)
        if  self.robot.drivetrain.turnToDegree(-73):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state10"

    def state10(self):
        self.onFirstRun(usesTiming = True)
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)
        self.robot.shooter.shoot(True)
        self.robot.drivetrain.setOutput(0, 0)
        if self.robot.stager.autoShoot(self.startTime, 3, self.robot.shooter.topError, self.robot.shooter.botError):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"

    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stopAllMotors()
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(False, False)
        self.robot.intake.intakeAndOutake(False, False, False)


    def run(self):
        try:
            print("(AutoY, run)  Current State: " + self.state)
            #print("Current Time - Start Time: " + str(time.time() - self.startTime))
            self.states[self.state]()

        except Exception:
            traceback.print_exc()

import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class autoZ(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        self.completedStep = False
        #States Are To Be Put Into This Dict
        self.states = {
            "state1":self.state1,
            "state2":self.state2,
            "state3":self.state3,
            "state4":self.state4,
            "state5":self.state5,
            "state6":self.state6,
            "state7":self.state7,
            "state8":self.state8,
            "state9":self.state9,
            "state10":self.state10,
            "state11":self.state11,
            "state12":self.state12,
            "state13":self.state13,
            "endState":self.endState
        }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.startTime = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
            "state1": False,
            "state2": False,
            "state3": False,
            "state4": False,
            "state5": False,
            "state6": False,
            "state7":False,
            "state8":False,
            "state9":False,
            "state10":False,
            "state11":False,
            "state12":False,
            "state13":False,
            "endState": False
        }

    def reset(self):
        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}
        self.state = self.initialState
        self.completedStep = False

    def state1(self):
        self.onFirstRun(usesTiming = True)
        self.robot.drivetrain.setOutput(0.2, -0.2)
        if not self.completedStep:
            pass
            #self.completedStep = self.robot.drivetrain.turnToDegree(10)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 0.1:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state2"
            self.completedStep = False

    def state2(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(-35)
        else:
            self.robot.drivetrain.stopAllMotors()
        self.robot.intake.intakeAndOutake(True, False, False)
        if time.time() - self.startTime >= 0.75:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(69)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 1.2:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state4"

    def state4(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        self.robot.drivetrain.setOutput(0.2, -0.2)
        if time.time() - self.startTime >= 0.25:
            self.robot.drivetrain.stopAllMotors()
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state5"

    def state5(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        self.robot.stager.updateSensorStates()
        self.robot.stager.shoot(False, True, self.robot.shooter.topError, self.robot.shooter.botError)

        if time.time() - self.startTime >= 1.5:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state6"

    def state6(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(False)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(-30)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 1:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state7"

    def state7(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)

        if not self.completedStep:
            self.completedStep = False
        else:
            self.robot.drivetrain.stopAllMotors()
        if self.robot.drivetrain.turnToDegree(60, 0.22, 0.12):
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state8"
            self.completedStep = False
    def state8(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.updateSensorStates()
        self.robot.stager.feedAndStage(True, False)

        if not self.completedStep:
            self.completedStep = False
        else:
            self.robot.drivetrain.stopAllMotors()
        if self.robot.drivetrain.driveToPosition(-70):
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state10"
            self.completedStep = False
    def state9(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.updateSensorStates()
        self.robot.stager.feedAndStage(True, False)
        if self.robot.drivetrain.driveToPosition(-20):
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state10"
            self.completedStep = False

    def state10(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(68)
        else:
            self.robot.drivetrain.stopAllMotors()
        self.robot.stager.updateSensorStates()
        self.robot.stager.feedAndStage(True, False)
        if time.time() - self.startTime >= 2:
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state11"
            self.completedStep = False

    def state11(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)

        if not self.completedStep:
            self.completedStep = False
        else:
            self.robot.drivetrain.stopAllMotors()
        if self.robot.drivetrain.turnToDegree(-56, 0.32, 0.14):
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state12"
            self.completedStep = False
    def state12(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)
        if self.robot.drivetrain.driveToPosition(38):
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state13"
            self.completedStep = False
    def state13(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.2, -0.2)
        self.robot.shooter.shoot(True)
        self.robot.stager.updateSensorStates()
        self.robot.stager.shoot(False, True, self.robot.shooter.topError, self.robot.shooter.botError)

        if time.time() - self.startTime >= 1.5:
            self.robot.drivetrain.stopAllMotors()
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"
            self.completedStep = False
    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stopAllMotors()


    def run(self):
        try:
            print("Current State: " + self.state)
            print("Current Time - Start Time: " + str(time.time() - self.startTime))
            print(time.time() - self.startTime >= 3)
            self.robot.drivetrain.printMotorOutputs()
            self.states[self.state]()

        except Exception:
            traceback.print_exc()

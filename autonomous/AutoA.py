import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class autoA(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        #States Are To Be Put Into This Dict
        self.states = {
                "state1":self.state1,
                "state2":self.state2,
                "state3":self.state3,
                "endState":self.endState
                }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.startTime = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
                "state1": False,
                "state2": False,
                "state3": False,
                "endState": False
                }
    def onFirstRun(self, *args, usesTiming = False):
        if not self.hasStateBeenRun[self.state]:
            if usesTiming:
                self.startTime = time.time()
            for method in args:
                method()
            self.hasStateBeenRun[self.state] = True

    def reset(self):
        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}
        self.state = self.initialState
        self.robot.drivetrain.stopAllMotors()
        self.robot.drivetrain.encoderAndGyroReset()

    def state1(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(True)
        if  self.robot.drivetrain.driveToPosition(28):
            self.state = "state2"
            self.robot.drivetrain.encoderAndGyroReset()

    def state2(self):
        self.onFirstRun(usesTiming = True)
        self.robot.drivetrain.setOutput(0, 0)
        self.robot.shooter.shoot(True)
        if  self.robot.stager.autoShoot(self.startTime, 3, self.robot.shooter.topError, self.robot.shooter.botError):
            self.state = "state3"
            self.robot.drivetrain.encoderAndGyroReset()

    def state3(self):
        self.onFirstRun(usesTiming = True)
        if  self.robot.drivetrain.driveToPosition(-70):
            self.state = "endState"
            self.robot.drivetrain.encoderAndGyroReset()

    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stopAllMotors()

    
    def run(self, debug : bool):
        try:
            if debug:
                print("Current State: " + self.state)
                print("Current Time - Start Time: " + str(time.time() - self.startTime))
                self.robot.drivetrain.printMotorOutputs()
            self.states[self.state]()
            
        except Exception:
            traceback.print_exc()

import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class defaultAuto(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        self.completedStep = False
        #States Are To Be Put Into This Dict
        self.states = {
                "state1":self.state1,
                "state2":self.state2,
                "state3":self.state3,
                "state4":self.state4,
                "state5":self.state5,
                "endState":self.endState
                }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.startTime = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
                "state1": False,
                "state2": False,
                "state3": False,
                "state4": False,
                "state5": False,
                "endState": False
                }

    def reset(self):
        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}
        self.state = self.initialState
        self.completedStep = False

    def state1(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.turnToDegree(10)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 1:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state2"
            self.completedStep = False

    def state2(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(-30)
        else:
            self.robot.drivetrain.stopAllMotors()
        self.robot.intake.intakeAndOutake(True, False, False)
        if time.time() - self.startTime >= 1:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.turnToDegree(-2)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 1:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state4"

    def state4(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(64)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 1.5:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state5"

    def state5(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        self.robot.shooter.shoot(True)
        self.robot.stager.updateSensorStates()
        self.robot.stager.shoot(False, True, self.robot.shooter.topError, self.robot.shooter.botError)

        if time.time() - self.startTime >= 5:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"

    def state7(self):
        self.onFirstRun(usesTiming = True)
        #self.robot.drivetrain.setOutput(0.5, 0.5)
        if not self.completedStep:
            self.completedStep = self.robot.drivetrain.driveToPosition(-36)
        else:
            self.robot.drivetrain.stopAllMotors()
        if time.time() - self.startTime >= 3:
            self.completedStep = False
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"
    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stopAllMotors()

    
    def run(self):
        try:
            print("Current State: " + self.state)
            print("Current Time - Start Time: " + str(time.time() - self.startTime))
            print(time.time() - self.startTime >= 3) 
            self.robot.drivetrain.printMotorOutputs()
            self.states[self.state]()
            
        except Exception:
            traceback.print_exc()

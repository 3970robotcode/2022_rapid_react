import traceback
import time

from .Auto_Base import AutoBase
from robot import Robot

class autoX(AutoBase):
    def __init__(self, robot: Robot):
        """Init Method for Auto Routine, All Motor and Sensor Reset Commands Or Component Definitions go here"""
        self.robot = robot
        #States Are To Be Put Into This Dict
        self.states = {
            "state1":self.state1,
            "state2":self.state2,
            "state3":self.state3,
            "state4":self.state4,
            "endState":self.endState
        }
        #Current State
        self.initialState = "state1"
        self.state = self.initialState
        self.startTime = 0
        #Has State Been Run (Important For Timing And The On First Run Method)
        self.hasStateBeenRun = {
            "state1": False,
            "state2": False,
            "state3": False,
            "state4": False,
            "endState": False
        }

    def onFirstRun(self, *args, usesTiming = False):
        if not self.hasStateBeenRun[self.state]:
            if usesTiming:
                self.startTime = time.process_time()
            for method in args:
                method()
            self.hasStateBeenRun[self.state] = True

    def reset(self):
        self.robot.drivetrain.encoderAndGyroReset()
        self.robot.drivetrain.stopAllMotors()

        self.hasStateBeenRun = {x: False for x in self.hasStateBeenRun}
        
        self.state = self.initialState
        
        print(self.state)
        
        print("Sensor Position", self.robot.drivetrain.rMaster.getSelectedSensorPosition())

    def state1(self):
        self.onFirstRun(self.robot.drivetrain.encoderAndGyroReset, usesTiming = True)
        self.robot.stager.updateSensorStates()
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)

        nextState = self.robot.drivetrain.driveToPosition(-38, True) and time.process_time() - self.startTime > 1
        # nextState = self.robot.drivetrain.driveToPositionVelocity(-45, 10000)
        print("(AutoX, state1) Next State", nextState)
        print("time:", time.process_time(), "\nstartTime:", self.startTime)
        if nextState:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state2"

    def state2(self):
        self.onFirstRun(usesTiming = True)
        self.robot.stager.updateSensorStates()
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)

        if self.robot.drivetrain.driveToPosition(70, True):
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state3"

    def state3(self):
        self.onFirstRun(usesTiming = True)
        self.robot.stager.updateSensorStates()
        self.robot.intake.intakeAndOutake(True, False, False)
        self.robot.stager.feedAndStage(True, False)
        self.robot.shooter.shoot(True)
        if time.process_time() - self.startTime > 0.5:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "state4"

    def state4(self):
        self.onFirstRun(usesTiming = True)
        self.robot.shooter.shoot(True)
        self.robot.drivetrain.setOutput(0, 0)
        self.robot.stager.updateSensorStates()
        self.robot.stager.shoot(False, True, self.robot.shooter.topError, self.robot.shooter.botError)
        if time.process_time() - self.startTime > 6:
            self.robot.drivetrain.encoderAndGyroReset()
            self.state = "endState"


    def endState(self):
        #End State Should ALways Stop All Motors
        self.robot.drivetrain.stopAllMotors()
        self.robot.shooter.shoot(False)
        self.robot.stager.feedAndStage(False, False)
        self.robot.intake.intakeAndOutake(False, False, False)
        self.robot.intake.intakeArmsUpAndDown(False, True)

    def run(self):
        try:
            # print("Current Time - Start Time: " + str(time.process_time() - self.startTime))
            self.states[self.state]()

        except Exception:
            traceback.print_exc()

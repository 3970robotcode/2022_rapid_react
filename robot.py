from math import sin, cos
import traceback
import time

import wpilib as w
# from cscore import CameraServer
import ctre

#Component, Autonomous, and Controller Import Statements
#from components import *
#from autonomous import *
import subsystems as s

import autonomous as a

#Constants File For Motor IDs, Joystick IDs, Etc...
from subsystems import Constants as c
from subsystems.drivetrain import Drivetrain

class Robot(w.TimedRobot):
    """FRC Robot"""
    def robotInit(self):
        """Robot-wide initialization code should go here"""
        self.lStick = w.Joystick(c.rStick)
        self.rStick = w.Joystick(c.lStick)

        self.wheel = w.Joystick(c.wheel)

        self.gamepad = w.Joystick(c.gamepad)
        
        self.drivetrain = s.Drivetrain()
        self.intake = s.Intake()
        self.stager = s.Stager()
        self.shooter = s.Shooter()
        self.climber = s.Climber()
        # self.auto = s.Auto()

        self.autonomous_modes = {
                # 0:a.defaultAuto(self),
                #1:a.testingAuto(self),
                2:a.autoX(self),
                3:a.autoY(self),
                4:a.autoZ(self),
                5:a.autoA(self),
                6:a.defaultAuto(self)
                }
        
        #Sendable Chooser For Auto Modes
        self.chooser = w.SendableChooser()
        self.chooser.setDefaultOption('Unnamed 4 ball auto', 6)
        #self.chooser.addOption('Testing', 1)
        self.chooser.addOption('Auto x',  2)
        self.chooser.addOption('Auto y',  3)
        self.chooser.addOption('Auto Z',  4)
        self.chooser.addOption('Auto A',  5)



        w.SmartDashboard.putData('Autonomous Mode Selector', self.chooser)

        #Sendable Chooser For Auto delay
        self.chooserDelay = w.SendableChooser()
        self.chooserDelay.setDefaultOption('No Delay', 0)
        self.chooserDelay.addOption('1 Second',   1)
        self.chooserDelay.addOption('2 Seconds',  2)
        self.chooserDelay.addOption('3 Seconds',  3)
        self.chooserDelay.addOption('4 Seconds',  4)
        self.chooserDelay.addOption('5 Seconds',  5)



        w.SmartDashboard.putData('Autonomous Mode Delay Selector', self.chooserDelay)

        self.debug = False
        w.SmartDashboard.putBoolean("Debug:", self.debug)

        w.SmartDashboard.putBoolean("Stager:", self.stager.stagerSensor.get())
        w.SmartDashboard.putBoolean("Feeder:", self.stager.feederSensor.get())

        w.SmartDashboard.putBoolean("Arms", False)
        
        # I think this is redundant, and even if its not, if you make the boolean
        # a boolean box, you can have green be extended and red retracted, which should 
        # be easier to understand at a glance.


        w.SmartDashboard.putString("Climber Direction:", "Extended")

        
        #w.SmartDashboard.putBoolean("Climber Direction:", self.climber.pivotDirection)

        #Compressor Code

        airCompressor = w.Compressor(1, w.PneumaticsModuleType.CTREPCM)
        airCompressor.enableDigital()

        #Camera Code

        w.CameraServer.launch("vision.py:main")
        #w.CameraServer.launch()
        '''
        cs = CameraServer.getInstance()
        cs.enableLogging()

        camera = cs.startAutomaticCapture()

        camera.setResolution(width, height)
        '''
        #w.SmartDashboard.putNumber("lMC",self.drivetrain.lMaster.getSupplyCurrent())
        #w.SmartDashboard.putNumber("lMS",self.drivetrain.lSlave.getSupplyCurrent())
        #w.SmartDashboard.putNumber("rMC",self.drivetrain.rMaster.getSupplyCurrent())
        #w.SmartDashboard.putNumber("rMS",self.drivetrain.rSlave.getSupplyCurrent())

    def robotPeriodic(self):
        """Called Every 20ms Regardless of Mode"""
        pass

    def disabledInit(self):
        """Called only at the beginning of disabled mode"""
        #sets the drivetrain motors to break mode to halt robot travel and then to coast mode 
        self.drivetrain.encoderAndGyroReset()
        self.climber.stopAllMotors()
        self.climber.constantVoltage = False

        self.drivetrain.lMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.lSlave.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rSlave.setNeutralMode(ctre.NeutralMode.Brake)
        
        time.sleep(0.5)

        self.drivetrain.lMaster.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.lSlave.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.rMaster.setNeutralMode(ctre.NeutralMode.Coast)
        self.drivetrain.rSlave.setNeutralMode(ctre.NeutralMode.Coast)

    def disabledPeriodic(self):
        """Called every 20ms in disabled mode"""
        
    def autonomousInit(self):
        """Called only at the beginning of autonomous mode"""
        self.debug = w.SmartDashboard.getBoolean("Debug:", False)
        self.mode = self.chooser.getSelected()

        self.delay = self.chooserDelay.getSelected()
        
        self.autonomous_modes[self.mode].reset()

        self.drivetrain.encoderAndGyroReset()

        self.drivetrain.lMaster.configVoltageCompSaturation(7.0)
        self.drivetrain.rMaster.configVoltageCompSaturation(7.0)

        self.drivetrain.rSlave.configVoltageCompSaturation(7.0)
        self.drivetrain.lSlave.configVoltageCompSaturation(7.0)

        self.drivetrain.lMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.lSlave.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rSlave.setNeutralMode(ctre.NeutralMode.Brake)

        self.drivetrain.shift(True, False)
        self.intake.intakeArmsUpAndDown(True, False)
        self.intake.intakeAndOutake(False, False, False)
        self.shooter.shoot(False)
        self.drivetrain.setOutput(0, 0)
        self.stager.shoot(False, False, 0, 0)

        if w.DriverStation.getAlliance() == w.DriverStation.Alliance.kRed:
            self.stager.isAllianceRed = True
        else:
            self.stager.isAllianceRed = False

        self.stager.stagerBallCorrect = True



        time.sleep(0.02)
        self.autoStartTime = time.time()

    def autonomousPeriodic(self):
        """Called every 20ms in autonomous mode"""
        if (time.time()- self.autoStartTime > self.delay):
            self.autonomous_modes[self.mode].run()
            #self.drivetrain.driveToPosition(-72)
            #self.drivetrain.printTemps()
            print(self.autonomous_modes[self.mode].state)
        #self.auto.run(self.drivetrain)

    def teleopInit(self):
        """Called only at the beginning of teleoperated mode"""
        self.debug = w.SmartDashboard.getBoolean("Debug:", False)
        self.drivetrain.encoderAndGyroReset()
        self.drivetrain.lMaster.configVoltageCompSaturation(14)
        self.drivetrain.rMaster.configVoltageCompSaturation(14)

        self.drivetrain.rSlave.configVoltageCompSaturation(14)
        self.drivetrain.lSlave.configVoltageCompSaturation(14)

        self.drivetrain.lMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.lSlave.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.drivetrain.rSlave.setNeutralMode(ctre.NeutralMode.Brake)

        if w.DriverStation.getAlliance() == w.DriverStation.Alliance.kRed:
            self.stager.isAllianceRed = True
        else:
            self.stager.isAllianceRed = False

        self.drivetrain.peakTemp = 0

        self.drivetrain.shift(True, False)
        self.climber.constantVoltage = False
        self.climber.reset()




    def teleopPeriodic(self):
        """Called every 20ms in teleoperated mode"""

        # Drivetrain
        try:

            lThrottle = self.lStick.getY() * .75
            rThrottle = self.rStick.getY() * .75

            self.drivetrain.setOutput(lThrottle, rThrottle)


            shiftUp = self.rStick.getRawButton(6)
            shiftDown = self.lStick.getRawButton(11)

            self.drivetrain.shift(shiftUp,shiftDown)

            #self.drivetrain.printPid()

            #w.SmartDashboard.putNumber("lMC",self.drivetrain.lMaster.getOutputCurrent())
            #w.SmartDashboard.putNumber("lMS",self.drivetrain.lSlave.getOutputCurrent())
            #w.SmartDashboard.putNumber("rMC",self.drivetrain.rMaster.getOutputCurrent())
            #w.SmartDashboard.putNumber("rMS",self.drivetrain.rSlave.getOutputCurrent())
            # self.drivetrain.printTemps()


        except Exception:
            traceback.print_exc()
        # Other

        try:
            intake = self.gamepad.getRawButton(c.gp["b"])
            outake = self.gamepad.getRawButton(c.gp["back"])
            superOutake = self.gamepad.getRawButton(c.gp["l_bumper"])
            armsDown = self.gamepad.getRawButton(c.gp["y"])
            armsUp = self.gamepad.getRawButton(c.gp["x"])
            stage = self.gamepad.getRawButton(c.gp["r_bumper"])
            feed = self.gamepad.getRawButton(c.gp["b"])
            shoot = self.gamepad.getRawButton(c.gp["a"])
            level = self.gamepad.getRawButton(c.gp["start"])
            climbl = self.gamepad.getRawAxis(c.gp["l_stick_y"])
            full = False #not (self.stager.stagerSensorState and self.stager.feederSensorState)

            self.stager.updateSensorStates()


            if self.gamepad.getRawAxis(c.gp["r_trigger"]) > 0:
                clawClose = True
            else:
                clawClose = False

            if self.gamepad.getRawAxis(c.gp["l_trigger"]) > 0:
                clawOpen = True
            else:
                clawOpen = False

            spin = False


            self.stager.updateSensorStates()
            self.intake.intakeAndOutake(intake and not self.intake.piston.get() == w.DoubleSolenoid.Value.kForward, outake or superOutake, full)
            self.intake.intakeArmsUpAndDown(intake or (outake or superOutake), not intake and (not (outake or superOutake)))

            if armsUp:
                w.SmartDashboard.putBoolean("Arms", False)
            elif armsDown:
                w.SmartDashboard.putBoolean("Arms", True)

            self.shooter.shoot(shoot)

            self.stager.feedAndStage(feed or shoot, stage)
            self.stager.shoot(feed, stage, self.shooter.topError, self.shooter.botError)
            self.stager.outake(outake, superOutake)

            w.SmartDashboard.putBoolean("Stager:", not self.stager.stagerSensorState)
            w.SmartDashboard.putBoolean("Feeder:", not self.stager.feederSensorState)

            if level:
                self.climber.level()
            else:
                self.climber.climb(climbl)
            #else:
            #    self.climber.stopAllMotors()
            self.climber.claw(clawClose, clawOpen)

            # this is definetly redundant you had that call elsewhere (though its now commented out
            # see the comment I made in subsytems\climber.py

            if self.climber.pivotDirection:
                w.SmartDashboard.putString("Climber Direction:", "Extended")
            else:
                w.SmartDashboard.putString("Climber Direction:", "Retracted")

            self.drivetrain.printTemps()

            #w.SmartDashboard.putBoolean("Arms", self.intake.piston.get())

            '''
            w.SmartDashboard.putNumber("bottomshooterspeed", self.shooter.bottomMotor.getSelectedSensorVelocity())
            w.SmartDashboard.putNumber("topshooterspeed", self.shooter.topMotor.getSelectedSensorVelocity())
            w.SmartDashboard.putNumber("bottomshootererror", self.shooter.botError)
            w.SmartDashboard.putNumber("topshootererror", self.shooter.topError)

            w.SmartDashboard.putNumber("topRpm", self.shooter.topMotor.getSelectedSensorVelocity()/2048*600)
            w.SmartDashboard.putNumber("bottomRpm", self.shooter.bottomMotor.getSelectedSensorVelocity()/2048*600)

            w.SmartDashboard.putNumber("topRpmerror", self.shooter.topMotor.getClosedLoopError()/2048*600)
            w.SmartDashboard.putNumber("bottomRpmerror", self.shooter.bottomMotor.getClosedLoopError()/2048*600)
            '''
            #self.drivetrain.printGyro()
        except Exception:
            traceback.print_exc()



    def testInit(self):
        """Called only at the beginning of test mode"""
        pass

    def testPeriodic(self):
        """Called every 20ms in test mode"""
        pass
 


if __name__ == "__main__":
    w.run(Robot)
 

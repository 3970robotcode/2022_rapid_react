from .constants import Constants as c
import ctre
import wpilib as w

class Climber():
    def __init__(self):
        self.leftMotor = ctre.WPI_TalonFX(c.leftClimber)
        self.rightMotor = ctre.WPI_TalonFX(c.rightClimber)

        self.leftMotor.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, 10)
        self.rightMotor.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, 10)

        self.leftMotor.setSelectedSensorPosition(0)
        self.rightMotor.setSelectedSensorPosition(0)

        self.rightSensor = w.DigitalInput(2)
        self.leftSensor = w.DigitalInput(3)

        self.leftMotor.setNeutralMode(ctre.NeutralMode.Brake)
        self.rightMotor.setNeutralMode(ctre.NeutralMode.Brake)

        self.claws = w.DoubleSolenoid(1, w.PneumaticsModuleType.CTREPCM,c.clawIn, c.clawOut)
        self.claws.set(w.DoubleSolenoid.Value.kReverse)
        self.state = 0
        self.threshold = 40
        self.held = False
        self.constantVoltage = False
        self.pivotDirection = False
        self.rLeveled = False
        self.lLeveled = False
        self.leftSensorState = True
        self.rightSensorState = True

    def getSensorStates(self):
        self.leftSensorState = self.leftSensor.get()
        self.rightSensorState = self.rightSensor.get()

    def climb(self, speed : float):
        self.getSensorStates()
        #print("R:", self.rightSensorState, "L:", self.leftSensorState)
        if abs(speed) > 0.1:
            self.rLeveled = False
            self.lLeveled = False

        if self.rightSensorState or speed < -0.1:
            if abs(self.rightMotor.getSelectedSensorPosition()) < 250000:
                self.rightMotor.set(ctre.ControlMode.PercentOutput, -speed)
            elif abs(self.rightMotor.getSelectedSensorPosition()) < 280000 or speed > 0:
                self.rightMotor.set(ctre.ControlMode.PercentOutput, -speed/1.5)
            else:
                self.rightMotor.set(ctre.ControlMode.PercentOutput, 0)
        elif not self.rightSensorState:
            self.rightMotor.set(ctre.ControlMode.PercentOutput, -0.09)
        else:
            self.rightMotor.set(ctre.ControlMode.PercentOutput, 0)

        if self.leftSensorState or speed < -0.1:
            if abs(self.leftMotor.getSelectedSensorPosition()) < 250000:
                self.leftMotor.set(ctre.ControlMode.PercentOutput, speed)
            elif abs(self.leftMotor.getSelectedSensorPosition()) < 280000 or speed > 0:
                self.leftMotor.set(ctre.ControlMode.PercentOutput, speed/1.5)
            else:
                self.leftMotor.set(ctre.ControlMode.PercentOutput, 0)
        elif not self.leftSensorState:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, 0.09)
        else:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, 0)
        if (not self.leftSensorState) and (not self.rightSensorState):
            self.leftMotor.setSelectedSensorPosition(0)
            self.rightMotor.setSelectedSensorPosition(0)
    def level(self):
        self.getSensorStates()
        # print(self.leftMotor.getOutputCurrent())

        if self.leftSensorState and not self.lLeveled:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, 0.40)
        else:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.lLeveled = True

        if self.rightSensorState and not self.rLeveled:
            self.rightMotor.set(ctre.ControlMode.PercentOutput, -0.40)
        else:
            self.rightMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.rLeveled = True

        if self.lLeveled and self.rLeveled:
            self.leftMotor.setSelectedSensorPosition(0)
            self.rightMotor.setSelectedSensorPosition(0)
            self.stopAllMotors()
            print("reset")
            return True

    def claw(self, close : bool, open : bool):
        if close:
            self.claws.set(w.DoubleSolenoid.Value.kForward)
            self.pivotDirection = False
        
        elif open:
            self.claws.set(w.DoubleSolenoid.Value.kReverse)
            self.pivotDirection = True

    def reset(self):
        self.state = 0
        self.claws.set(w.DoubleSolenoid.Value.kReverse)

    def autoClimb(self):
        match self.state:
            case 0:
                if self.level():
                    self.state = 1
            case 1:
                if self.tarmsToPosition(250000):
                    self.state = 2
            case _:
                self.stopAllMotors()

    def tarmsToPosition(self, goalPosition):
        leftTicksToTurn = -goalPosition - self.leftMotor.getSelectedSensorPosition()
        rightTicksToTurn = goalPosition - self.rightMotor.getSelectedSensorPosition()

        if abs(leftTicksToTurn) > 1000:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, self.sign(leftTicksToTurn) * 0.35)
            left = False
        else:
            self.leftMotor.set(ctre.ControlMode.PercentOutput, 0)
            left = True
        if abs(rightTicksToTurn) > 1000:
            right = False
            self.rightMotor.set(ctre.ControlMode.PercentOutput, self.sign(rightTicksToTurn) * 0.35)
        else:
            self.rightMotor.set(ctre.ControlMode.PercentOutput, 0)
            right = True
        if right and left:
            return True
    
    def sign(self, number):
        return 1 if number >= 0 else -1

    def stopAllMotors(self):
        self.leftMotor.set(ctre.ControlMode.PercentOutput, 0)
        self.rightMotor.set(ctre.ControlMode.PercentOutput, 0)

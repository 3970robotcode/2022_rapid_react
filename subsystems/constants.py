class Constants():
    # Define Constants here
    # I.E. Motor S, Button S, P Tuning Values, etc

    # Motor Ids

    # Drivetrain
    dtRMaster = 2
    dtRSlave = 3

    dtLMaster = 0
    dtLSlave = 1

    # Intake

    intake = 4

    # Stager

    feeder = 5
    stager = 6

    # Shooter

    topShooter = 8
    bottomShooter = 7

    # CLimber

    leftClimber = 9
    rightClimber = 10

    # Controllers
    lStick = 0
    rStick = 1
    gamepad = 2
    wheel = 5

    # Pid Loops
    timeoutMS = 10

    lDrivetrainPID = {
        "kF": 0.0,
        "kP": 0.02,
        "kI": 0,
        "kD": 0.3,
        "kIZone": 10
    }

    rDrivetrainPID = {
        "kF": 0.0,
        "kP": 0.02,
        "kI": 0,
        "kD": 0.35,
        "kIZone": 10
    }


    drivetrainPIDVelocity = {
        "lkF": 0.4,
        "lkP": 0.03,
        "lkI": 0,
        "lkD": 0,

        "rkF": 0.4,
        "rkP": 0.035,
        "rkI": 0,
        "rkD": 0
    }

    tShooterPID = {
        "kF": 0.047953125,  # 0.000195
        "kP": 0.2,  # 0.001000 # 0.000252 # 8e-3,9.350e-5
        "kI": 0,  # 0.000001 # 0.000001 #0.000002,1.61e-6
        "kD": 10,  # 1.500000 # 0.002500 # 0.009,9.650e-5
        "kIZone": 10
    }

    bShooterPID = {
        "kF": 0.04890057361,  # 0.000195
        "kP": 0.2,  # 0.001000#0.000252#8e-3,9.350e-5
        "kI": 0,  # 0.000001 # 0.000001 # 0.000002, 1.61e-6
        "kD": 10,  # 1.500000 # 0.002500 # 0.009, 9.650e-5
        "kIZone": 10,
    }

    # Solenoids

    dtIn = 0

    dtOut = 1

    clawIn = 4

    clawOut = 5



    # Gamepad buttons
    gp = {
        "r_stick_x": 4, "r_stick_y": 5, "l_stick_x": 0, "l_stick_y": 1,
        "r_trigger": 3, "l_trigger": 2, "x": 3, "a": 1,
        "y": 4, "b": 2, "back": 7, "start": 8,
        "r_bumper": 6, "l_bumper": 5, "l_stick": 9, "r_stick": 10}

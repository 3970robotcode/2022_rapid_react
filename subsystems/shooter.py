from .constants import Constants as c
import ctre
from .util import configTalonFX
import wpilib as w

class Shooter():
    def __init__(self):
        self.bottomMotor = ctre.WPI_TalonFX(c.topShooter)
        self.topMotor = ctre.WPI_TalonFX(c.bottomShooter)

        configTalonFX(self.topMotor, PID_vals=c.tShooterPID)
        configTalonFX(self.bottomMotor, PID_vals=c.bShooterPID)
        '''
        self.bottomMotor.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, 10)
        self.topMotor.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, 10)

        self.bottomMotor.configPeakOutputForward(1, 10)
        self.bottomMotor.configPeakOutputReverse(-1, 10)

        self.topMotor.configPeakOutputForward(1, 10)
        self.topMotor.configPeakOutputReverse(-1, 10)

        self.bottomMotor.config_kF(0, c.shooterPID['bkF'], 10)
        self.bottomMotor.config_kP(0, c.shooterPID['bkP'], 10)
        self.bottomMotor.config_kD(0, c.shooterPID['bkD'], 10)

        self.topMotor.config_kF(0, c.shooterPID['tkF'], 10)
        self.topMotor.config_kP(0, c.shooterPID['tkP'], 10)
        self.topMotor.config_kD(0, c.shooterPID['tkD'], 10)
        '''
        self.bottomMotor.setNeutralMode(ctre.NeutralMode.Coast)
        self.topMotor.setNeutralMode(ctre.NeutralMode.Coast)

        self.topError = 0
        self.botError = 0


        self.topMotor.setSelectedSensorPosition(0)
        self.bottomMotor.setSelectedSensorPosition(0)


    def rpmToTicks(self, targetRpm):
        return(targetRpm*2048/600)

    def shoot(self, shoot : bool):
        '''
        if wrongColor:
            self.bottomMotor.set(ctre.ControlMode.Velocity, -self.rpmToTicks(3000))
            self.topMotor.set(ctre.ControlMode.Velocity, self.rpmToTicks(3000))

        el'''
        if shoot:
            self.bottomMotor.set(ctre.ControlMode.Velocity, -self.rpmToTicks(2300)) #2100 or 2300
            self.topMotor.set(ctre.ControlMode.Velocity, self.rpmToTicks(1.166*2500)) #1.166*2400 or 2500

            #good shot rpm with coulson wheels: 2400 and 1.166*2400

            self.topError = self.topMotor.getClosedLoopError()
            self.botError = self.bottomMotor.getClosedLoopError()

        else:
            self.bottomMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.topMotor.set(ctre.ControlMode.PercentOutput, 0)

from .constants import Constants as c
import ctre
import wpilib as w
import time
import rev

class Stager():
    def __init__(self):
        self.feederMotor = ctre.WPI_TalonSRX(c.feeder)
        self.stagerMotor = ctre.WPI_TalonSRX(c.stager)
        self.timesHeld = 0

        self.feederSensor = w.DigitalInput(0)
        self.stagerSensor = w.DigitalInput(1)
        #self.colorSensor = rev.ColorSensorV3(w.I2C.Port.kOnboard)

        self.stagerSensorState = False
        self.feederSensorState = False
        '''
        self.color = self.colorSensor.getColor()
        self.isBallRed = False
        self.isBallCorrect = False
        self.previousFeederSensorState = False
        self.previousStagerSensorState = False

        self.stagerBallCorrect = False
        self.feederBallCorrect = False
        self.previousFeederBallCorrect = False
        '''




    
    def updateSensorStates(self):
        #self.previousFeederSensorState = self.feederSensorState
        #self.previousStagerSensorState = self.stagerSensorState
        self.stagerSensorState = self.stagerSensor.get()
        self.feederSensorState = self.feederSensor.get()
        '''
        if self.previousFeederSensorState and (not self.feederSensorState):
            self.testBall()
            self.feederBallCorrect = self.isBallCorrect

        if (not self.previousFeederSensorState) and self.feederSensorState:
            self.previousFeederBallCorrect = self.feederBallCorrect

        if self.previousStagerSensorState and (not self.stagerSensorState):
            self.stagerBallCorrect = self.previousFeederBallCorrect

        if self.stagerSensorState and self.feederSensorState:
            self.stagerBallCorrect = True
        '''




    def feedAndStage(self, feed : bool, shoot : bool):
        if feed and not shoot:
            # This is redundant and probably adds overhead
            if self.stagerSensorState:
                self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.2)
            else:
                self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)

            if self.feederSensorState:
                if self.stagerSensorState:
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.45)
                else:
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.35)
            else:
                if self.stagerSensorState:
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.45)
                else:
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, 0)
        
        elif not shoot:

            self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.feederMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.timesHeld = 0

        #if not self.stagerBallCorrect:
        #    self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)

    def shoot(self, feed: bool, shoot: bool, topError : bool, botError : bool):
        if shoot and not feed:
            if not self.stagerSensorState:
                self.timesHeld = self.timesHeld + 1
                # print("sattempts")
                if abs(topError) <= 300 and abs(botError) <= 300 and self.timesHeld >= 10  and self.timesHeld <= 40:
                    self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.4)
                else:
                    self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)
                    self.feederMotor.set(ctre.ControlMode.PercentOutput, 0)
                if self.timesHeld >= 50:
                    self.timesHeld = 0
            else:
                self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.4)
                self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)
                self.timesHeld = 0
            if self.feederSensorState:
                self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.4)
        elif not feed:
            self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.feederMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.timesHeld = 0
        #if not self.stagerBallCorrect:
        #    self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)



    def outake(self, outake : bool, superOutake : bool):
        if outake:
            self.feederMotor.set(ctre.ControlMode.PercentOutput, 1)
        elif superOutake:
            self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0.75)
            self.feederMotor.set(ctre.ControlMode.PercentOutput, 1)
    def autoShoot(self, startTime, setTime, topError, botError):
        if time.process_time() - startTime >= setTime:
            return True
            self.feederMotor.set(ctre.ControlMode.PercentOutput, 0)
            self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)

        else:
            if not self.stagerSensorState:
                # self.timesHeld = self.timesHeld + 1
                if abs(topError) <= 400 and abs(botError) <= 400:
                    self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)
                else:
                    self.stagerMotor.set(ctre.ControlMode.PercentOutput, 0)
            else:
                self.feederMotor.set(ctre.ControlMode.PercentOutput, -0.5)
                self.stagerMotor.set(ctre.ControlMode.PercentOutput, -0.5)

            return False
    def testBall(self):
        self.color = self.colorSensor.getColor()
        if (self.color.red > self.color.blue):
            self.isBallRed = True
        else:
            self.isBallRed = False
        self.isBallCorrect = (self.isBallRed == self.isAllianceRed)

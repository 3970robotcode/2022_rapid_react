import ctre
# from collections import namedtuple
from ctre import WPI_TalonFX


# currentLimit = namedtuple('currentLimit', ['currentLimit', 'thresholdCurrent', 'thresholdTime', 'enable'])

def configTalonFX(motor: WPI_TalonFX, peakOutputForward=1, peakOutputReverse=-1, PID_vals:dict={"kF":0,"kP":0,"kI":0,"kD":0,"kIZone":0}, currentLimit:dict={"currentLimit":35,"thresholdCurrent":38,"thresholdTime":0.002,"enable":True}, feedbackDevice:ctre.FeedbackDevice=ctre.FeedbackDevice.IntegratedSensor):

    mConfig = ctre.TalonFXConfiguration()

    mConfig.supplyCurrLimit.currentLimit = currentLimit["currentLimit"]
    mConfig.supplyCurrLimit.triggerThresholdCurrent = currentLimit["thresholdCurrent"]
    mConfig.supplyCurrLimit.triggerThresholdTime = currentLimit["thresholdTime"]
    mConfig.supplyCurrLimit.enable = currentLimit["enable"]

    mConfig.slot0.kF = PID_vals["kF"]
    mConfig.slot0.kP = PID_vals["kP"]
    mConfig.slot0.kI = PID_vals["kI"]
    mConfig.slot0.kD = PID_vals["kD"]
    mConfig.slot0.integralZone = PID_vals["kIZone"]

    mConfig.peakOutputForward = peakOutputForward
    mConfig.peakOutputReverse = peakOutputReverse

    motor.configAllSettings(mConfig)

    motor.configSelectedFeedbackSensor(feedbackDevice)


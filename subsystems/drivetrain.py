from .constants import Constants as c
import wpilib as w
import ctre
from .util import configTalonFX
import math

import time
import math

class Drivetrain(): 
    """Drive Train"""
   
    def __init__(self):
        """Motor And Sensor Initialization"""
        self.rMaster = ctre.WPI_TalonFX(c.dtRMaster)
        self.rSlave = ctre.WPI_TalonFX(c.dtRSlave)
        
        self.lMaster = ctre.WPI_TalonFX(c.dtLMaster)
        self.lSlave = ctre.WPI_TalonFX(c.dtLSlave)
        
        self.rSlave.set(ctre.ControlMode.Follower, c.dtRMaster)
        self.lSlave.set(ctre.ControlMode.Follower, c.dtLMaster)

        self.gyro = w.ADXRS450_Gyro()

        configTalonFX(self.lMaster, PID_vals=c.lDrivetrainPID)
        configTalonFX(self.lSlave, PID_vals=c.lDrivetrainPID)
        configTalonFX(self.rMaster, PID_vals=c.rDrivetrainPID)
        configTalonFX(self.rSlave, PID_vals=c.rDrivetrainPID)


        '''

        self.lMaster.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.lSlave.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.rMaster.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)
        self.rSlave.configSelectedFeedbackSensor(ctre.FeedbackDevice.IntegratedSensor, 0, c.timeoutMS)

        self.lMaster.config_kF(0, c.drivetrainPID["lkF"])
        self.lMaster.config_kP(0, c.drivetrainPID["lkP"])
        self.lMaster.config_kI(0, c.drivetrainPID["lkI"])
        self.lMaster.config_kD(0, c.drivetrainPID["lkD"])

        self.rMaster.config_kF(0, c.drivetrainPID["rkF"])
        self.rMaster.config_kP(0, c.drivetrainPID["rkP"])
        self.rMaster.config_kI(0, c.drivetrainPID["rkI"])
        self.rMaster.config_kD(0, c.drivetrainPID["rkD"])
        
        self.rMaster.configPeakOutputForward(1.0)
        self.rMaster.configPeakOutputReverse(-1.0)
        
        self.lMaster.configPeakOutputForward(1.0)
        self.lMaster.configPeakOutputReverse(-1.0)
        
        self.lSlave.configPeakOutputReverse(-1.0)
        self.lSlave.configPeakOutputForward(1.0)
        
        self.rSlave.configPeakOutputReverse(-1.0)
        self.rSlave.configPeakOutputForward(1.0)
        '''

        self.rMaster.configClosedLoopPeakOutput(0, 0.4, c.timeoutMS)
        self.rSlave.configClosedLoopPeakOutput(0, 0.4, c.timeoutMS)
        self.lMaster.configClosedLoopPeakOutput(0, 0.4, c.timeoutMS)
        self.lSlave.configClosedLoopPeakOutput(0, 0.4, c.timeoutMS)



        #self.rMaster.configClosedLoopPeakOutput(0, 1, c.timeoutMS)
        #self.lMaster.configClosedLoopPeakOutput(0, 1, c.timeoutMS)
         
        self.rSlave.set(ctre.ControlMode.Follower, c.dtRMaster)
        self.lSlave.set(ctre.ControlMode.Follower, c.dtLMaster)
        
        self.lMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.lSlave.setNeutralMode(ctre.NeutralMode.Brake)
       
        self.rMaster.setNeutralMode(ctre.NeutralMode.Brake)
        self.rSlave.setNeutralMode(ctre.NeutralMode.Brake)


        '''
        self.rMaster.configSupplyCurrentLimit(currentLimit, c.timeoutMS)
        self.rSlave.configSupplyCurrentLimit(currentLimit, c.timeoutMS)
        self.lMaster.configSupplyCurrentLimit(currentLimit, c.timeoutMS)
        self.lSlave.configSupplyCurrentLimit(currentLimit, c.timeoutMS)
        '''
        '''
        self.rMaster.CurrentLimitConfiguration(True, 38, 35, 0.001)
        self.lMaster.supplyCurrentLimitConfiguration(True, 38, 35, 0.001)
        self.rSlave.supplyCurrentLimitConfiguration(True, 38, 35, 0.001)
        self.lSlave.supplyCurrentLimitConfiguration(True, 38, 35, 0.001)
        '''

        self.driveShift = w.DoubleSolenoid(1, w.PneumaticsModuleType.CTREPCM, c.dtIn, c.dtOut)

        self.driveShift.set(w.DoubleSolenoid.Value.kReverse)
        
        self.isForward = True
        
        self.held = False
        
        self.previousError = 0

        self.peakTemp = 0



    def sign(self, number):
        return 1 if number >= 0 else -1

    def setOutput(self, lSpeed: float, rSpeed: float):
        # TODO: Is this Needed?
        #lSpeed = max(rSpeed,lSpeed) if abs(lSpeed-rSpeed) < 0.05 else lSpeed

        self.lMaster.set(ctre.ControlMode.PercentOutput, -lSpeed)
        self.rMaster.set(ctre.ControlMode.PercentOutput, rSpeed)

    def setVelocity(self, ticksPerMil):
        self.rMaster.set(ctre.ControlMode.Velocity, ticksPerMil)
        self.lMaster.set(ctre.ControlMode.Velocity, -ticksPerMil)

    def shift(self, shiftUp : bool, shiftDown : bool):

        if shiftUp:
            self.driveShift.set(w.DoubleSolenoid.Value.kForward)
        elif shiftDown:
            self.driveShift.set(w.DoubleSolenoid.Value.kReverse)

    def stopAllMotors(self): 
        self.setOutput(0,0)
        return True
 
    def printMotorOutputs(self):
        print("lMaster Output" + str(self.lMaster.getMotorOutputPercent()))
        print("rMaster Output" + str(self.rMaster.getMotorOutputPercent()))

    def printMotorCurrents(self):
        print("lMaster Current" + str(self.lMaster.getOutputCurrent()))
        print("lSlave Current" + str(self.lSlave.getOutputCurrent()))
        print("rMaster Current" + str(self.rMaster.getOutputCurrent()))
        print("rSlave Current" + str(self.rSlave.getOutputCurrent()))


    def encoderAndGyroReset(self):
        self.lMaster.setSelectedSensorPosition(0, 0, c.timeoutMS)
        self.rMaster.setSelectedSensorPosition(0, 0, c.timeoutMS)
        self.gyro.reset()
        self.previousError = 0

    def driveToPosition(self, inches: int, debug: bool = False):
        rotations = inches/(math.pi * 5)
        targetTicks = rotations * 11042

        if (debug):
            print("(drivetrain, driveToPosition) Target Ticks:", targetTicks)
            
            print(  "1 (drivetrain, driveToPosition) Closed loop error R:", 
                    self.rMaster.getClosedLoopError(), 
                    "manual:", 
                    abs(targetTicks - self.rMaster.getSelectedSensorPosition()))

        self.lMaster.set(ctre.ControlMode.Position, -targetTicks)
        self.rMaster.set(ctre.ControlMode.Position, targetTicks)

        if (debug):
            print(  "2 (drivetrain, driveToPosition) Closed loop error R:", 
                    self.rMaster.getClosedLoopError(), 
                    "manual:", 
                    abs(targetTicks - self.rMaster.getSelectedSensorPosition()))

        if (abs(targetTicks - self.rMaster.getSelectedSensorPosition()) <= 1200):
            self.rMaster.set(ctre.ControlMode.PercentOutput, 0)
            self.lMaster.set(ctre.ControlMode.PercentOutput, 0)
            return True
       
        else:
            return False


    def driveToPositionVelocity(self, inches: int, velocity : float, debug: bool = False):
        rotations = inches/(math.pi * 5)
        targetTicks = rotations * 11042
        
        if debug:
            print("(drivetrain, driveToPosition) Target Ticks:", targetTicks)

            print(  "1 (drivetrain, driveToPosition)",
                    "distError:",
                    abs(targetTicks - self.rMaster.getSelectedSensorPosition()))

            print(  "1 (drivetrain, driveToPosition)",
                    "currentPos:",
                    abs(self.rMaster.getSelectedSensorPosition()))

            print(  "1 (drivetrain, driveToPosition)",
                    "Switch State",
                    abs(targetTicks - self.rMaster.getSelectedSensorPosition()) <= 1200)

        self.lMaster.set(ctre.ControlMode.Velocity, velocity)
        self.rMaster.set(ctre.ControlMode.Velocity, -velocity)

        if (abs(targetTicks - self.rMaster.getSelectedSensorPosition()) <= 1200):
            self.rMaster.set(ctre.ControlMode.PercentOutput, 0)
            self.lMaster.set(ctre.ControlMode.PercentOutput, 0)
            return True

        else:
            return False


    def turnToDegree(self, goalAngle : float, bigVoltage : float, smallVoltage : float, debug : bool = True):
        currentAngle = self.gyro.getAngle()
        degreesToTurn = goalAngle - currentAngle
        
        if (debug):
            print("(drivetrain, turnToDegree) Setpoint:", goalAngle)
            print("(drivetrain, turnToDegree) Current Angle:", self.gyro.getAngle())
        
        if (abs(degreesToTurn) < 0.25):
            if debug: print("Success")
            return True
        
        else:
            #self.rMaster.set(ctre.ControlMode.PercentOutput, -(self.sign(degreesToTurn)*0.17) - min(0.3, degreesToTurn/180))
            #self.lMaster.set(ctre.ControlMode.PercentOutput, -(self.sign(degreesToTurn)*0.17) - min(0.3, degreesToTurn/180))
            if degreesToTurn/goalAngle < 0.27:
                self.rMaster.set(ctre.ControlMode.PercentOutput, -self.sign(degreesToTurn)*smallVoltage)
                self.lMaster.set(ctre.ControlMode.PercentOutput, -self.sign(degreesToTurn)*smallVoltage)
            else:
                self.rMaster.set(ctre.ControlMode.PercentOutput, -self.sign(degreesToTurn)*bigVoltage)
                self.lMaster.set(ctre.ControlMode.PercentOutput, -self.sign(degreesToTurn)*bigVoltage)
            return False

    def printPid(self):
        # 1 wheel rotation in high gear is 11,042 ticks
        print("r Position:" + str(self.rMaster.getSelectedSensorPosition()))
        print("l Position:" + str(self.lMaster.getSelectedSensorPosition()))
        print("r Error:" + str(self.rMaster.getClosedLoopError()))
        print("l Error:" + str(self.lMaster.getClosedLoopError()))

    def printGyro(self):
        print(self.gyro.getAngle())

    def printTemps(self):
        w.SmartDashboard.putNumber('temperatureLMaster', self.lMaster.getTemperature())
        w.SmartDashboard.putNumber('temperatureLSlave', self.lSlave.getTemperature())
        w.SmartDashboard.putNumber('temperatureRMaster', self.rMaster.getTemperature())
        w.SmartDashboard.putNumber('temperatureRSlave', self.rSlave.getTemperature())

        self.peakTemp = max(max(max(self.lMaster.getTemperature(), self.lSlave.getTemperature()), max(self.rMaster.getTemperature(), self.rSlave.getTemperature())), self.peakTemp)
        print("HigestTemp:", self.peakTemp)

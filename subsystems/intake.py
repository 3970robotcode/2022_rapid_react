from .constants import Constants as c
import ctre
import wpilib as w

class Intake():
    def __init__(self):
        self.motor = ctre.WPI_TalonSRX(c.intake)
        self.piston = w.DoubleSolenoid(1, w.PneumaticsModuleType.CTREPCM, 2, 3)
        self.piston.set(w.DoubleSolenoid.Value.kReverse)

        self.direction = True


    def intakeAndOutake(self, intake : bool, outake : bool, full : bool):

        if intake and not full:
            self.motor.set(ctre.ControlMode.PercentOutput, 0.9)
        elif outake or (intake and full):
            self.motor.set(ctre.ControlMode.PercentOutput, -1)
        else:
            self.motor.set(ctre.ControlMode.PercentOutput, 0)

    def intakeArmsUpAndDown(self, down : bool, up : bool):
        if down and not self.direction:
            #self.piston.set(False)
            self.piston.set(w.DoubleSolenoid.Value.kReverse)
            self.direction = True
        elif up and self.direction:
            #self.piston.set(True)
            self.piston.set(w.DoubleSolenoid.Value.kForward)
            self.direction = False
